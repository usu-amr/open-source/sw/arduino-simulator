#pragma once

class Servo{
public:
  Servo();
  void attach(int pin);
  void writeMicroseconds(int value);

private:
  int pin;
};
