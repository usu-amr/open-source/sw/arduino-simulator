#pragma once

#include <functional>
#include <string>
#include <vector>

extern "C" void attachToBridge(std::function<void(std::string, std::string, std::vector<std::string>)> callback);
extern "C" void deliverFromBridge(std::string scope, std::string method, std::vector<std::string> args);

void receiveFromBridge(std::string scope, std::string method, std::function<void(std::vector<std::string>)> callback);
void sendToBridge(std::string scope, std::string method, std::vector<std::string> args);

class BridgeSubscription{
public:
  BridgeSubscription(std::string scope, std::string method, std::function<void(std::vector<std::string>)> callback){
    receiveFromBridge(scope, method, callback);
  }
};
