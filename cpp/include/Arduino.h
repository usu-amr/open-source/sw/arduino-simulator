#pragma once

// stop name mangling
extern "C" void setup();
extern "C" void loop();

#include <cstdint>
#include <cstddef>

#include <pins_arduino.hpp>
#include <HardwareSerial.hpp>

#define HIGH 0x1
#define LOW  0x0

#define INPUT 0x0
#define OUTPUT 0x1

extern HardwareSerial Serial;

void pinMode(uint8_t, uint8_t);
void digitalWrite(uint8_t, uint8_t);
int digitalRead(uint8_t);
void analogWrite(uint8_t, int);

unsigned long millis(void);
void delay(unsigned long);

#define abs(x) ((x)>0?(x):-(x))
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
