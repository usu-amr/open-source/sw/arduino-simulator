#pragma once

#include <cstdint>
#include <cstddef>

#define SERIAL_RX_BUFFER_SIZE 64

class HardwareSerial{
public:
  void begin(uint32_t baud);
  void end();
  uint16_t available();
  int read();
  size_t write(const char *buffer, size_t size);
  operator bool();
};
