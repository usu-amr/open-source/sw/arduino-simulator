#include <bridge.hpp>
#include <Arduino.h>

using namespace std;

#include <iostream>

HardwareSerial Serial;




struct BridgeHandler{
  std::string scope;
  std::string method;
  function<void(vector<string>)> callback;
};

function<void(string, string, vector<string>)> bridgeCallback;
vector<BridgeHandler> handlers;

void sendToBridge(std::string scope, std::string method, std::vector<std::string> args){
  bridgeCallback(scope, method, args);
}

void receiveFromBridge(std::string scope, std::string method, function<void(vector<string>)> callback){
  handlers.push_back({scope, method, callback});
}

void attachToBridge(function<void(string, string, vector<string>)> callback){
  bridgeCallback = callback;
}

void deliverFromBridge(std::string scope, std::string method, std::vector<std::string> args){
  for(unsigned int i = 0; i < handlers.size(); ++i){
    if(handlers[i].scope.compare(scope) == 0 && handlers[i].method.compare(method) == 0){
      handlers[i].callback(args);
    }
  }
}

extern void initHandlersWiringAnalog();
extern void initHandlersWiringDigital();
extern void initHandlersWiring();
extern void initHandlersHardwareSerial();

extern "C" void initHandlers();

void initHandlers(){
  initHandlersWiringDigital();
  initHandlersWiringAnalog();
  initHandlersWiring();
  initHandlersHardwareSerial();
}
