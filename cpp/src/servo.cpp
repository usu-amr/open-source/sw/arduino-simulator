#include <Servo.h>
#include <bridge.hpp>

using namespace std;

Servo::Servo()
: pin(-1){

}

void Servo::attach(int pin){
  this->pin = pin;
  sendToBridge("servo", "attach", {to_string(pin)});
}

void Servo::writeMicroseconds(int value){
  sendToBridge("servo", "writeMicroseconds", {to_string(this->pin), to_string(value)});
}
