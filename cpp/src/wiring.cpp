#include <bridge.hpp>
#include <chrono>
#include <thread>

using namespace std;
using namespace std::chrono;


void initHandlersWiring(){

}


high_resolution_clock::time_point startTime(high_resolution_clock::now());

unsigned long millis(){
  high_resolution_clock::time_point now = high_resolution_clock::now();
  unsigned long deltaTime = chrono::duration_cast<chrono::milliseconds>(now - startTime).count();
  sendToBridge("core", "millis", {to_string(deltaTime)});
  return deltaTime;
}

void delay(unsigned long ms){
  sendToBridge("core", "delay", {to_string(ms)});
  this_thread::sleep_for(chrono::milliseconds(ms));
}
