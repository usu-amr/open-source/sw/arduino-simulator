#include <HardwareSerial.hpp>

#include <bridge.hpp>

#include <base64.hpp>

#include <queue>

using namespace std;

void HardwareSerial::begin(uint32_t baud){
  sendToBridge("serial", "begin", {to_string(baud)});
}

void HardwareSerial::end(){
  sendToBridge("serial", "end", {});
}

queue<uint8_t> readBuffer;

uint16_t HardwareSerial::available(){
  uint16_t length = readBuffer.size();
  sendToBridge("serial", "available", {to_string(length)});
  return length;
}

HardwareSerial::operator bool(){
  return true;
}

int HardwareSerial::read(){
  uint8_t byte = readBuffer.front();
  readBuffer.pop();
  sendToBridge("serial", "read", {to_string(byte)});
  return byte;
}

int writeLength = -1;

size_t HardwareSerial::write(const char *buffer, size_t size){
  size_t writtenLength = (writeLength < 0 ? size : writeLength);
  sendToBridge("serial", "write", {base64_encode((unsigned char*)(void*)(buffer), size), to_string(size), to_string(writtenLength)});
  return writtenLength;
}

void readBufferSet(vector<string> args){
  string buffer = base64_decode(args.at(0));
  for(int i = 0; i < buffer.length(); ++i){
    readBuffer.push(buffer.at(i));
  }
}

void writeLengthSet(vector<string> args){
  writeLength = stoi(args.at(0));
}

void initHandlersHardwareSerial(){
  receiveFromBridge("serial", "readBufferSet", readBufferSet);
  receiveFromBridge("serial", "writeLengthSet", writeLengthSet);
}
