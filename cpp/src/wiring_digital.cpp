#include <bridge.hpp>

#define DIGITAL_PIN_COUNT 100

using namespace std;

void pinMode(uint8_t pin, uint8_t mode){
  sendToBridge("core", "pinMode", {to_string(pin), to_string(mode)});
}

void digitalWrite(uint8_t pin, uint8_t level){
  sendToBridge("core", "digitalWrite", {to_string(pin), to_string(level)});
}



vector<int> digitalPinStates(DIGITAL_PIN_COUNT);

void digitalReadSet(vector<string> args){
  digitalPinStates.at(stoi(args[0])) = stoi(args[1]);
}

// BridgeSubscription digitalReadSub("core", "digitalReadSet", digitalReadSet);

int digitalRead(uint8_t pin){
  sendToBridge("core", "digitalRead", {to_string(pin), to_string(digitalPinStates.at(pin))});
  return digitalPinStates.at(pin);
}


void initHandlersWiringDigital(){
  receiveFromBridge("core", "digitalReadSet", digitalReadSet);
}
