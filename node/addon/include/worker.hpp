#pragma once

#include <nan.h>

NAN_METHOD(powerOn);
NAN_METHOD(send);
NAN_METHOD(load);
NAN_METHOD(unload);

NAN_METHOD(listen);
NAN_METHOD(powerOff);
NAN_METHOD(isOn);
NAN_METHOD(isLoaded);
