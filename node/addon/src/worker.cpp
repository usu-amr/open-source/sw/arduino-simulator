#include <nan.h>
#include <functional>
#include <string>
#include <vector>
#include <iostream>
#include <dlfcn.h>

#include <safe_queue.hpp>

using namespace std;

using v8::FunctionTemplate;
using v8::Handle;
using v8::Object;
using v8::String;
using Nan::GetFunction;
using Nan::New;
using Nan::Set;


using v8::Local;
using v8::Function;
using v8::Isolate;
using v8::Exception;
using v8::Number;
using v8::Value;
using v8::Array;
using Nan::Null;
using Nan::Callback;


using Nan::AsyncQueueWorker;
using Nan::AsyncProgressQueueWorker;
using Nan::AsyncWorker;
using Nan::HandleScope;
using Nan::To;




typedef void (*SetupFunc)(void);
typedef void (*LoopFunc)(void);
typedef void (*InitHandlersFunc)(void);
typedef void (*AttachToBridgeFunc)(function<void(string, string, vector<string>)> callback);
typedef void (*DeliverFromBridgeFunc)(std::string scope, std::string method, std::vector<std::string> args);

void* loadedSketch = nullptr;
bool isSketchActive = false;
Callback* bridgeCallback = nullptr;
Callback* stopCallback = nullptr;

SetupFunc setup = nullptr;
LoopFunc loop = nullptr;
InitHandlersFunc initHandlers = nullptr;
AttachToBridgeFunc attachToBridge = nullptr;
DeliverFromBridgeFunc deliverFromBridge = nullptr;

NAN_METHOD(load) {
  if(loadedSketch == nullptr){
    string sketchPath = *Nan::Utf8String(info[0].As<String>());

    cout << "Sketch Path: " << sketchPath << endl;

    loadedSketch = dlopen(sketchPath.c_str(), RTLD_NOW);
    char* error = dlerror();
    if(error != NULL){
      cout << "Error? " << error << endl;
    }

    setup = (SetupFunc)dlsym(loadedSketch, "setup");
    error = dlerror();
    if(error != NULL){
      cout << "Error? " << error << endl;
    }

    loop = (LoopFunc)dlsym(loadedSketch, "loop");
    error = dlerror();
    if(error != NULL){
      cout << "Error? " << error << endl;
    }

    attachToBridge = (AttachToBridgeFunc)dlsym(loadedSketch, "attachToBridge");
    error = dlerror();
    if(error != NULL){
      cout << "Error? " << error << endl;
    }

    deliverFromBridge = (DeliverFromBridgeFunc)dlsym(loadedSketch, "deliverFromBridge");
    error = dlerror();
    if(error != NULL){
      cout << "Error? " << error << endl;
    }

    initHandlers = (InitHandlersFunc)dlsym(loadedSketch, "initHandlers");
    error = dlerror();
    if(error != NULL){
      cout << "Error? " << error << endl;
    }

    initHandlers();

  }else{
    cout << "Sketch already loaded" << endl;
  }
}

NAN_METHOD(unload) {
  if(loadedSketch != nullptr){
    cout << "Unloading sketch..." << endl;
    dlclose(loadedSketch);
    loadedSketch = nullptr;
  }else{
    cout << "No sketch loaded" << endl;
  }

  char* error = dlerror();
  if(error != NULL){
    cout << "Error? " << error << endl;
  }
}






struct BridgeSentInfo{
  string scope;
  string method;
  vector<string> args;
};

SafeQueue<BridgeSentInfo> sendToSimulator;
SafeQueue<bool> simulatorStop;

class BridgeWorker : public AsyncProgressQueueWorker<BridgeSentInfo> {
 public:
  BridgeWorker(Callback* finsihedCallback)
    : AsyncProgressQueueWorker<BridgeSentInfo>(finsihedCallback) {
      isSketchActive = true;
    }
  ~BridgeWorker() {}

  // Executed inside the worker-thread.
  // It is not safe to access V8, or V8 data structures
  // here, so everything we need for input and output
  // should go on `this`.
  void Execute(const ExecutionProgress& progress) {
    attachToBridge([&progress](string scope, string method, vector<string> args){
      BridgeSentInfo info = {scope, method, args};
      progress.Send(&info, 1);
    });

    this->updateSimulator();

    setup();

    while(simulatorStop.empty()){
      this->updateSimulator();

      loop();
    }

    simulatorStop.dequeue();
  }

  void updateSimulator(){
    unsigned int count = 0;
    while(!sendToSimulator.empty() && count++ < 10){
      BridgeSentInfo info = sendToSimulator.dequeue();
      deliverFromBridge(info.scope, info.method, info.args);
    }
  }

  // Executed when the async work is complete
  // this function will be run inside the main event loop
  // so it is safe to use V8 again
  void HandleOKCallback() {
    HandleScope scope;

    isSketchActive = false;

    this->callback->Call(0, nullptr);

    if(stopCallback != nullptr){
      stopCallback->Call(0, nullptr);
    }
  }

  void HandleProgressCallback(const BridgeSentInfo* data, size_t count){
    HandleScope scope;

    Local<Array> nodeArgs = New<Array>(data->args.size());
    for(int i = 0; i < data->args.size(); ++i){
      Set(nodeArgs, i, New<String>(data->args[i]).ToLocalChecked());
    }

    Local<Value> argv[] = {
      New<String>(data->scope).ToLocalChecked(),
      New<String>(data->method).ToLocalChecked(),
      nodeArgs
    };

    bridgeCallback->Call(3, argv);
  }
};

NAN_METHOD(powerOn) {
  Callback* finishedCallback = new Callback(info[0].As<Function>());

  AsyncQueueWorker(new BridgeWorker(finishedCallback));
}

NAN_METHOD(send) {
  HandleScope scope_;

  string scope = *Nan::Utf8String(info[0].As<String>());
  string method = *Nan::Utf8String(info[1].As<String>());

  Local<Value> lengthProp = New<String>("length").ToLocalChecked();

  int length = To<int>(Nan::Get(info[2].As<Array>(), lengthProp).ToLocalChecked()).FromJust();
  vector<string> args;
  for(unsigned int i = 0; i < length; ++i){
    string arg = *Nan::Utf8String(Nan::Get(info[2].As<Array>(), i).ToLocalChecked());
    args.push_back(arg);
  }

  sendToSimulator.enqueue({scope, method, args});
}



NAN_METHOD(listen) {
  // maybe clean up the old callback pointer if there is one
  bridgeCallback = new Callback(info[0].As<Function>());
}

NAN_METHOD(powerOff) {
  if(isSketchActive){
    stopCallback = new Callback(info[0].As<Function>());
    simulatorStop.enqueue(true);
  }
}

NAN_METHOD(isOn) {
  info.GetReturnValue().Set(isSketchActive);
}

NAN_METHOD(isLoaded) {
  info.GetReturnValue().Set(loadedSketch != nullptr);
}
