#include <nan.h>
#include <functional>
#include <string>
#include <vector>
#include <iostream>
#include <string>

#include <worker.hpp>


using namespace std;

using v8::FunctionTemplate;
using v8::Handle;
using v8::Object;
using v8::String;
using Nan::GetFunction;
using Nan::New;
using Nan::Set;


using v8::Local;
using v8::Function;
using v8::Isolate;
using v8::Exception;
using v8::Number;
using v8::Value;
using v8::Array;
using Nan::Null;
using Nan::Callback;

NAN_MODULE_INIT(Initialize) {
  Set(target, New<String>("powerOn").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(powerOn)).ToLocalChecked());

  Set(target, New<String>("send").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(send)).ToLocalChecked());

  Set(target, New<String>("load").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(load)).ToLocalChecked());

  Set(target, New<String>("unload").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(unload)).ToLocalChecked());

  Set(target, New<String>("listen").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(listen)).ToLocalChecked());

  Set(target, New<String>("powerOff").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(powerOff)).ToLocalChecked());

  Set(target, New<String>("isOn").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(isOn)).ToLocalChecked());

  Set(target, New<String>("isLoaded").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(isLoaded)).ToLocalChecked());
}

NODE_MODULE(addon, Initialize)
