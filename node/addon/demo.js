const arduino = require('bindings')('arduino-simulator');

arduino.load('../../../bots/tijar/arduino/build/libarduino.so')

console.log('Arduino starting...')

let count = 0

arduino.powerOn((namespace, method, args) => {

  if(method === 'millis' || method === 'delay' || method === 'digitalRead') return;

  if(method === 'pinMode'){
    args[1] = (args[1] === '1' ? 'OUTPUT' : 'INPUT')
  }

  if(method === 'digitalWrite'){
    args[1] = (args[1] === '1' ? 'HIGH' : 'LOW')
  }

  if(namespace === 'core'){
    namespace = ''
  }else{
    namespace = namespace + '.'
  }

  console.log('%s%s(%s)', namespace, method, args.join(', '))
}, () => {
  console.log('Arduino done')
})

let state = false
setInterval(() => {

  state = !state;

  arduino.send('core', 'digitalReadSet', ['2', (state ? '1' : '0')]);

}, 1000)
