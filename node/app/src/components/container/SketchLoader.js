import { FilePicker } from 'react-file-picker'
import React, { Component } from "react";

const SketchLoader = ({ onChange }) => (
  <FilePicker
    extensions={['so', 'dll']}
    onChange={(fileObject) => onChange(fileObject)}
    onError={(errMsg) => console.log(errMsg)}
  >
    <button className="button">
      Load New Sketch
    </button>
  </FilePicker>
)

export default SketchLoader
