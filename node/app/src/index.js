import io from 'socket.io-client'
import ReactDOM from "react-dom"
import React from "react";

import SketchLoader from "./components/container/SketchLoader.js";

const socket = io('http://localhost:3000')

socket.on('test', (msg) => {
  console.log('message: ' + msg)
})

window.socket = socket

const wrapper = document.getElementById("content");

class IsActive extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      isActive: false
    }

    socket.on('connect', (event) => {
      this.setState({
        isActive: true
      })
    })

    socket.on('disconnect', (event) => {
      this.setState({
        isActive: false
      })
    })
  }

  render(){
    return <h1>
      {this.state.isActive ? "Connected" : "Disconnected"}
    </h1>
  }
}

class Log extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      events: []
    }

    socket.on('event', (event) => {
      // console.log('got event', event)
      this.setState({
        events: [
          ...this.state.events.slice(-200),
          event
        ]
      })
    })
  }

  render(){
    const wrapped = this.state.events.map((event, i) => <li key={i}>{event}</li>)
    return <ul>
      {wrapped.reverse()}
    </ul>
  }
}

class Send extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      value: []
    }
  }

  render(){
    return <div>
      <input className="input" onChange={(e) => this.setState({value: e.target.value})} value={this.state.value}/>
      <button className="button" onClick={() => socket.emit('arduino.send', JSON.parse(this.state.value))}>
        Send
      </button>
    </div>
  }
}

let serialMsg = ""

ReactDOM.render(<div>
  <IsActive/>
  <SketchLoader
    onChange={(fileObject) => {
      // console.log(fileObject)
      socket.emit('arduino.load', fileObject)
    }}
  />
  <button className="button" onClick={() => socket.emit('arduino.reload')}>
    Load Sketch
  </button>
  <button className="button" onClick={() => socket.emit('arduino.start')}>
    Start Sketch
  </button>
  <button className="button" onClick={() => socket.emit('arduino.stop')}>
    Stop Sketch
  </button>
  <br/>
  <Send/>
  <br/>
  <Log/>
</div>, wrapper)
