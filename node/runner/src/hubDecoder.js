const { scan, filter, map } = require('rxjs/operators')
const { pipe } = require('rxjs')

const defaultMessage = () => ({check: null, length: null, valueIndex: null, done: false, values: []})

const decodeHubMessages = () => pipe(
  scan((acc, curr) => {
    if(acc.done){
      acc = defaultMessage()
    }
    if(acc.check === null){
      if(curr === 1){
        acc.check = curr
      }
    }else if(acc.length === null){
      acc.length = curr
      acc.valueIndex = 0
    }else{
      if(!acc.values[acc.valueIndex]){
        acc.values.push({value: [], length: curr})
      }else{
        acc.values[acc.valueIndex].value.push(curr)

        acc.valueIndex = (acc.values[acc.valueIndex].value.length >= acc.values[acc.valueIndex].length ? acc.valueIndex + 1 : acc.valueIndex)
        acc.done = acc.valueIndex >= acc.length
      }
    }
    return acc
  }, defaultMessage()),
  filter(message => message.done),
  map(message => message.values.map(data => Buffer.from(data.value)))
)

module.exports = {
  decodeHubMessages
}
