const arduino = require('bindings')('../../addon/build/Release/arduino-simulator')
const { Observable } = require('rxjs')
const { publish, refCount } = require('rxjs/operators')
const fs = require('fs')

const SKETCH_PATH = 'temp/arduino.sketch'

if(!fs.existsSync('temp')){
  fs.mkdirSync('temp')
}

const events$ = Observable.create((observer) => {
  arduino.listen((scope, method, args) => {
    observer.next({scope, method, args})
  })
}).pipe(
  publish(),
  refCount()
)

const loadNew = (buffer) => {
  return new Promise((resolve, reject) => {
    if(!arduino.isOn()){
      if(arduino.isLoaded()){
        arduino.unload()
      }

      fs.writeFile(SKETCH_PATH, buffer, (err) => {
        if(err){
          reject(err)
        }else{
          arduino.load(SKETCH_PATH)
          resolve()
        }
      })
    }else{
      reject(Error('Arduino is on'))
    }
  })
}

const load = () => {
  return new Promise((resolve, reject) => {
    if(!arduino.isOn()){
      if(arduino.isLoaded()){
        arduino.unload()
      }

      arduino.load(SKETCH_PATH)
      resolve()
    }else{
      reject()
    }
  })
}

const start = () => {
  return new Promise((resolve) => {
    if(!arduino.isOn()){
      arduino.powerOn(() => {})
    }
    resolve()
  })
}

const stop = () => {
  return new Promise((resolve) => {
    if(arduino.isOn()){
      arduino.powerOff(() => {
        resolve()
      })
    }else{
      resolve()
    }
  })
}

const send = ({scope, method, args}) => {
  return new Promise((resolve) => {
    arduino.send(scope, method, args)
    resolve()
  })
}

module.exports = {
  events$,
  loadNew,
  load,
  start,
  stop,
  send
}
