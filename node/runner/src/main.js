const arduino = require('./arduino');
const { decodeHubMessages } = require('./hubDecoder')

const http = require('http').Server()
const io = require('socket.io')(http)

const { filter, map, mergeMap } = require('rxjs/operators')

const SerialPort = require('serialport')


const events$ = arduino.events$.pipe(
  filter(({ scope, method }) => scope !== 'serial'),
  filter(({ scope, method }) => method !== 'millis'),
  map(({ scope, method, args }) => `${scope}.${method}(${args.join(', ')})`)
)

const serialOut$ = arduino.events$.pipe(
  filter(({ scope, method }) => scope === 'serial' && method === 'write'),
  map(({ args }) => Buffer.from(args[0], 'base64').slice(0, parseInt(args[2])))
)

const decodedSerial$ = serialOut$.pipe(
  mergeMap(buffer => [...buffer]),
  decodeHubMessages(),
)


const serialPort = new SerialPort('/tmp/virtualcom1', { baudRate: 9600, lock: false })

serialOut$.subscribe({
  next: buffer => serialPort.write(buffer)
})

serialPort.on('data', data => {
  arduino.send({
    scope:'serial',
    method:'readBufferSet',
    args:[data.toString('base64')]
  })
})

const port = 3000

io.on('connection', (socket) => {
  console.log('a user connected');

  decodedSerial$.subscribe({
    next: data => {
      const name = data[0].toString()
      socket.emit('event', `Serial - ${name}${data.length > 1 ? ', ' : ''}${data.slice(1).map(binary => binary.toString('hex'))}`)
    }
  })

  events$.subscribe({
    next: data => {
      socket.emit('event', data)
    }
  })

  socket.on('arduino.load', (file) => {
    arduino.loadNew(file).then(() => {
      socket.emit('arduino.loaded')
    })
  });

  socket.on('arduino.reload', () => {
    arduino.load().then(() => {
      socket.emit('arduino.loaded')
    })
  })

  socket.on('arduino.stop', () => {
    arduino.stop().then(() => {
      socket.emit('arduino.stopped')
    })
  })

  socket.on('arduino.start', () => {
    arduino.start().then(() => {
      socket.emit('arduino.started')
    })
  })

  socket.on('arduino.send', ({ scope, method, args }) => {
    arduino.send({scope, method, args})
  })

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(port, () => {
  console.log(`listening on *:${port}`);
});
